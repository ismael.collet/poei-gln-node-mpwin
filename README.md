# POEI - GLN - node-mpwin

## Description
Projet permettant de déployer une application Node.js avec GitLab CI/CD, dans le cadre de la formation DevOps d'ORSYS.

## Installation
Ce projet est censé être lancé par GitLab CD/CI

## Utilisation
Une fois le projet déployer, l'application doit être accessible par navigateur.

## Auteur et reconnaissance
Projet développé par Ismaël Collet, il est basé sur un TP de Achraf Bannour.

## Project status
En développement.
